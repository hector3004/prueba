<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('respuesta');
		$this->load->helper('select');
		$this->load->helper('validacion');
	}	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function modelos(){
		$post = $this->input->post();
		$marca = $post['marca'];
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://private-7475d3-benditocoche.apiary-mock.com/asesores/libroazul/tipo.json?m={marca}");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		//CURLOPT_CUSTOMREQUEST => "POST",
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		$response = curl_exec($ch);
		var_dump($response );exit();
		curl_close($ch);

		var_dump($response);
	}

}
