<?php
	/**
	* Extraer una parte de un array asociativo
	*
	* @package Helpers
	* @subpackage
	* @category Array
	* @author Jesus Soto
	* @link http://ejemplo.com
 	*/
	defined('BASEPATH') OR exit('No direct script access allowed');
	if( !function_exists('array_slice_assoc') ){
	   	function array_slice_assoc($array,$keys) {
	    	return array_intersect_key($array,array_flip($keys));
		}
	}
?>

