<?php

/**
 * Validacion
 *
 * @package Helpers
 * @subpackage
 * @category Aritmetica
 * @author Daniel uribe
 * @link http://ejemplo.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Funcion para validar redondear los valores
 * @param string El numero a redondear
 * @param int El numero de decimales a redondear.
 * @return bool Regresa el numero redondeado a la escala definida
 */
if(!function_exists('bcround')){
    function bcround($number, $scale=2) {
        $fix = "5";
        for ($i=0;$i<$scale;$i++) $fix="0$fix";
            $number = bcadd($number, "0.$fix", $scale+1);
            return    bcdiv($number, "1.0",    $scale);
        }
}

if(!function_exists('bcaddround')){
    function bcaddround($number, $number2, $scale=4) {
        return bcround(bcadd($number,$number2,$scale),2);
    }
}

if(!function_exists('bcmulround')){
    function bcmulround($number, $number2, $scale=4) {
        return bcround(bcmul($number,$number2,$scale),2);
    }
}

if(!function_exists('bcdivround')){
    function bcdivround($number, $number2, $scale=4) {
        return bcround(bcdiv($number,$number2,$scale),2);
    }
}

if(!function_exists('bcsubround')){
    function bcsubround($number, $number2, $scale=4) {
        return bcround(bcsub($number,$number2,$scale),2);
    }
}