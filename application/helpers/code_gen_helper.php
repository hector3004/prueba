<?php

/**
 * Code Generator
 *
 * @package Helpers
 * @subpackage
 * @category Codegen
 * @author Cesar Gonzalez
 * @link http://mlp.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if( !function_exists('generar_codigo') ){
    function generar_codigo($format,$current){
        $arr = explode("%", $format, 2);
        $prefix = $arr[0];
        $sufix = $arr[1];
        $zero_delete = ltrim($sufix, '0');
        $digits = explode("d", $zero_delete, 2);
        $digits = $digits[0];
        $number = str_pad($current, $digits, '0', STR_PAD_LEFT);
        $code = $prefix.$number;
        return $code;
    }
}



