<?php
/**
	* Elimina arrays multidimencionales duplicados
	*
	* @package Helpers
	* @subpackage
	* @category Array
	* @author Jesus Soto
	* @link
 */
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists ('elementos_unicos')){
	function elementos_unicos($array)
	{
	    $arraySinDuplicados = [];
	    foreach($array as $indice => $elemento) {
	        if (!in_array($elemento, $arraySinDuplicados)) {
	            $arraySinDuplicados[$indice] = $elemento;
	        }
	    }
	    return $arraySinDuplicados;
	}	
}
