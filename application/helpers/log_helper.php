<?php

/**
 * Log Guardar Helper
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if(!function_exists('guardar_actividad')){
    function guardar_actividad($id_usuario,$id_log){
        $CI =& get_instance();
        $CI->load->database(); 
        $actividad = array(
            'id_usuario' => $id_usuario,
            'id_log' => $id_log
        );
        $CI->db->insert('usuarios_acciones', $actividad);
    }
}
