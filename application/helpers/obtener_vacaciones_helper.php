<?php

/**
 * Obtener vacaciones
 *
 * @package Helpers
 * @subpackage
 * @category Select
 * @author Daniel uribe
 * @link http://ejemplo.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * *****************************************************************************************************************
 * ******************************************************DEPENDENCIAS***********************************************
 * *****************************************************************************************************************
 * REQUIERE EJECUTAR el método generar_vacaciones_empleado_barcos, que es parte de Calculo_model
 * 
 * 
 * Calcula las vacaciones pendientes y del año corriente del empleado
 * $arg_dataIn array de entrada con información fecha_posible_baja, fecha_antiguedad, fecha_aniversario, id_empleado, 
 * @return bool Regresa falso en caso de que algun parametro sea vacio, verdadero en caso contrario
 */
if( !function_exists('f_obtenerVacacionesEmpleado') ){
    function f_obtenerVacacionesEmpleado($arg_dataIn){
        $ls_fecha_posible_baja = $arg_dataIn['fecha_posible_baja'];
        $ls_fecha_antiguedad =  $arg_dataIn['fecha_antiguedad'];
        $ls_fecha_aniversario = $arg_dataIn['fecha_aniversario']; 
        $li_dias_vacaciones_derecho = $arg_dataIn['dias_vacaciones_derecho'];        
        //$this->mCalculo->generar_vacaciones_empleado_barcos($li_id_empleado, $li_id_operacion, $ls_fecha_posible_baja);
        
        $ls_fecha_posible_baja = $ls_fecha_posible_baja;
        $ls_fecha_antiguedad = $ls_fecha_antiguedad;
        $ls_fecha_aniversario = $ls_fecha_aniversario;
        $li_dias_transcurridos = f_cantidadDiasVacaciones($ls_fecha_antiguedad, $ls_fecha_posible_baja);

        $li_total_anios = 0;
        $li_total_anios = bcdiv($li_dias_transcurridos, 365, 0);
        $li_dias_reales_anios = bcmul($li_total_anios, 365, 0);
        $li_dias_diferencia = $li_dias_transcurridos - $li_dias_reales_anios;                
        $li_dias_vacaciones_proporcional = bcmul(bcdiv($li_dias_diferencia, 365, 5), $li_dias_vacaciones_derecho, 5);

        $la_diasDerecho =  array();                
        $li_anio_corriente = 0;
        //Considerar para factor de integración
        $li_anio_corriente  = bcadd($li_total_anios, 1, 0);                

        if($li_total_anios == 0){
            $la_diasDerecho = f_tablaVacaciones(0);
            $li_vacaciones_pendientes = $la_diasDerecho['dias'];
        }else{
            $la_diasDerecho = f_tablaVacaciones( ($li_total_anios+1));
            $li_vacaciones_pendientes = $la_diasDerecho['dias'] ;
        }

        $la_dataOut['vacaciones_pendientes'] = $li_vacaciones_pendientes;
        $la_dataOut['vacaciones_pendientes_text'] = $li_vacaciones_pendientes;

        $la_dataOut['vacaciones_antiguedad']  = $li_dias_vacaciones_proporcional;
        $la_dataOut['vacaciones_antiguedad_text']  = f_tablaVacaciones( ($li_anio_corriente +1) )['dias'];
        
        return $la_dataOut;
    }
}



/**
 * Calcula los días transcurridos entre dos fechas
 * $ls_fecha_antiguedad, $ls_fecha_posible_baja, fecha inicial y fecha termino, en el mismo orden en formato aaaa-mm-dd
 * @return int, con la cantidad de días transcurridos.
 */
if( !function_exists('f_diferenciaDiasFechas') ){
    function f_cantidadDiasVacaciones($ls_fecha_antiguedad, $ls_fecha_posible_baja){
        $f_antiguedad = $ls_fecha_antiguedad;
        $fecha_antiguedad = new DateTime($f_antiguedad);
        $fecha_hoy = new DateTime($ls_fecha_posible_baja);

        $diferencia = $fecha_hoy->diff($fecha_antiguedad);

        $anios_antiguedad = ($diferencia->y <= 0) ? 0 : $diferencia->y;
        return $diferencia->days;
    }
}


/**
 * Tabla de vacaciones
 * $arg_posicion int
 * $diasPorAnio array, con la cantidad de días correspondientes.
 */
if( !function_exists('f_tablaVacaciones') ){
    function f_tablaVacaciones($arg_posicion){
        $diasPorAnio = array();
        $diasPorAnio[0] = ["anio" => 0, "dias" => 0, "derechoDias" => 0];
        $diasPorAnio[1] = ["anio" => 1, "dias" => 6, "derechoDias" => 6];
        $diasPorAnio[2] = ["anio" => 2, "dias" => 8, "derechoDias" => 14];
        $diasPorAnio[3] = ["anio" => 3, "dias" => 10, "derechoDias" => 24];
        $diasPorAnio[4] = ["anio" => 4, "dias" => 12, "derechoDias" => 36];
        $diasPorAnio[5] = ["anio" => 5, "dias" => 14, "derechoDias" => 50];
        $diasPorAnio[6] = ["anio" => 6, "dias" => 14, "derechoDias" => 64];
        $diasPorAnio[7] = ["anio" => 7, "dias" => 14, "derechoDias" => 78];
        $diasPorAnio[8] = ["anio" => 8, "dias" => 14, "derechoDias" => 92];
        $diasPorAnio[9] = ["anio" => 9, "dias" => 14, "derechoDias" => 106];
        $diasPorAnio[10] = ["anio" => 10, "dias" => 16, "derechoDias" => 122];
        $diasPorAnio[11] = ["anio" => 11, "dias" => 16, "derechoDias" => 138];
        $diasPorAnio[12] = ["anio" => 12, "dias" => 16, "derechoDias" => 154];
        $diasPorAnio[13] = ["anio" => 13, "dias" => 16, "derechoDias" => 170];
        $diasPorAnio[14] = ["anio" => 14, "dias" => 16, "derechoDias" => 186];
        $diasPorAnio[15] = ["anio" => 15, "dias" => 18, "derechoDias" => 204];
        $diasPorAnio[16] = ["anio" => 16, "dias" => 18, "derechoDias" => 222];
        $diasPorAnio[17] = ["anio" => 17, "dias" => 18, "derechoDias" => 240];
        $diasPorAnio[18] = ["anio" => 18, "dias" => 18, "derechoDias" => 258];
        $diasPorAnio[19] = ["anio" => 19, "dias" => 18, "derechoDias" => 276];
        $diasPorAnio[20] = ["anio" => 20, "dias" => 20, "derechoDias" => 296];
        $diasPorAnio[21] = ["anio" => 21, "dias" => 20, "derechoDias" => 316];
        $diasPorAnio[22] = ["anio" => 22, "dias" => 20, "derechoDias" => 336];
        $diasPorAnio[23] = ["anio" => 23, "dias" => 20, "derechoDias" => 356];
        $diasPorAnio[24] = ["anio" => 24, "dias" => 20, "derechoDias" => 376];
        $diasPorAnio[25] = ["anio" => 25, "dias" => 22, "derechoDias" => 398];
        $diasPorAnio[26] = ["anio" => 26, "dias" => 22, "derechoDias" => 420];
        $diasPorAnio[27] = ["anio" => 27, "dias" => 22, "derechoDias" => 442];
        $diasPorAnio[28] = ["anio" => 28, "dias" => 22, "derechoDias" => 464];
        $diasPorAnio[29] = ["anio" => 29, "dias" => 22, "derechoDias" => 486];
        $diasPorAnio[30] = ["anio" => 30, "dias" => 24, "derechoDias" => 510];
        $diasPorAnio[31] = ["anio" => 31, "dias" => 24, "derechoDias" => 534];
        $diasPorAnio[32] = ["anio" => 32, "dias" => 24, "derechoDias" => 558];
        $diasPorAnio[33] = ["anio" => 33, "dias" => 24, "derechoDias" => 582];
        $diasPorAnio[34] = ["anio" => 34, "dias" => 24, "derechoDias" => 606];
        
        if($arg_posicion > 1){
            $arg_posicion = $arg_posicion - 1;
        }
        return ((isset($diasPorAnio[$arg_posicion]))?$diasPorAnio[$arg_posicion]:$diasPorAnio[34]);
    }
}