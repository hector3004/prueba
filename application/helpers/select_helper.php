<?php

/**
 * Validacion
 *
 * @package Helpers
 * @subpackage
 * @category Select
 * @author Daniel uribe
 * @link http://ejemplo.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Funcion para validar que varios parametros no sean vacios si alguno es vacío regresa FALSE
 * @param mixed Número infinito de parametros
 * @return bool Regresa falso en caso de que algun parametro sea vacio, verdadero en caso contrario
 */
if( !function_exists('generar_html_select') ){
    function generar_html_select($array,$campo_value,$campo_label,$pintarValorVacio = FALSE,$valorDefault = NULL){
        $html = '';
        if($pintarValorVacio){
            $html.= '<option value="">Seleccionar...</option>';
        }
        if($array){
            foreach($array as $a){
	            if( $valorDefault && ($a[$campo_value] == $valorDefault) ){
		            $html.=sprintf('<option value="%s" selected>%s</option>', $a[$campo_value],$a[$campo_label]);
	            }else
	                $html.=sprintf('<option value="%s">%s</option>', $a[$campo_value],$a[$campo_label]);
            }
        }
        return $html;
    }
}

/**
 * Funcion para validar que varios parametros no sean vacios si alguno es vacío regresa FALSE
 * @param mixed Número infinito de parametros
 * @return bool Regresa falso en caso de que algun parametro sea vacio, verdadero en caso contrario
 */
if( !function_exists('generar_html_select_data') ){
    function generar_html_select_data($array,$campo_value,$campo_label,$campos_data=FALSE,$pintarValorVacio = FALSE){
        $html = '';
        if($pintarValorVacio){
            $html.= '<option value="">Seleccionar...</option>';
        }
        if($campos_data){
            $arreglo_data = explode(',',$campos_data);
            foreach($array as $a){
                $data = '';
                foreach($arreglo_data as $d){
                    $data .= sprintf(" data-%s = '%s'", $d, $a[$d]);
                }
                $html.=sprintf('<option value="%s" %s >%s</option>',$a[$campo_value],$data,$a[$campo_label]);
            }
        }
        return $html;
    }
}


