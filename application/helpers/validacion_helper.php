<?php

/**
 * Validacion
 *
 * @package Helpers
 * @subpackage
 * @category Validaciones
 * @author Daniel uribe
 * @link http://ejemplo.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Funcion para validar que varios parametros no sean vacios si alguno es vacío regresa FALSE
 * @param mixed Número infinito de parametros
 * @return bool Regresa falso en caso de que algun parametro sea vacio, verdadero en caso contrario
 */
if(!function_exists('validar_vacios')){
    function validar_vacios(){
        foreach(func_get_args() as $param){
            if(!isset($param) || trim($param)==='')
                return FALSE;
        }
        return TRUE;
    }
}

/**
 * Función para limpiar los datos de un arreglo, quita los vacíos y los espacios de mas
 * @param mixed Arreglo con valores
 * @return array Regresa el nuevo arreglo sin los valores vacios y trimmeados.
 */
if( !function_exists('limpiar_arreglo') ){
    function limpiar_arreglo($arreglo_datos){
        if( is_array($arreglo_datos) ){
            $new_arr = (array_map('trim',$arreglo_datos));
        }
        return $new_arr;
    }
}

/**
* Funcion para recorrer un arreglo recursivamente
*/
if( !function_exists('array_map_recursive') ){
    function array_map_recursive($callback, $array){
        $func = function ($item) use (&$func, &$callback) {
            return is_array($item) ? array_map($func, $item) : call_user_func($callback, $item);
        };
        return array_map($func, $array);
    }
}

/**
 * Función para validar que un valor sea numerico y positivo
 * @param mixed Arreglo con valores
 * @return array Regresa el nuevo arreglo sin los valores vacios y trimmeados.
 */
if( !function_exists('es_entero_positivo') ){
    function es_entero_positivo(){
        foreach(func_get_args() as $param){
            if(!isset($param) || trim($param)==='' || !(is_integer($param) && (int) $param >= 0) )
                return FALSE;
        }
        return TRUE;
    }
}
