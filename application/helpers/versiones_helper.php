<?php

/**
 * Validacion
 *
 * @package Helpers
 * @subpackage
 * @category Validaciones
 * @author Daniel uribe
 * @link http://ejemplo.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Funcion para validar que varios parametros no sean vacios si alguno es vacío regresa FALSE
 * @param mixed Número infinito de parametros
 * @return bool Regresa falso en caso de que algun parametro sea vacio, verdadero en caso contrario
 */
if(!function_exists('guardar_version_tabla')){
    function guardar_registro_version($tabla,$condicion_registro,$id_usuario){
        $tabla_version = $tabla.'_versiones';
        $CI = & get_instance();
        $CI->db->from($tabla);
        $CI->db->where($condicion_registro)->limit(1);
        $query = $CI->db->get();
        $registro = $query->row_array();
        $registro['id_usuario'] = $id_usuario;
        unset($registro['id_usuario_alta']);
        $CI->db->insert($tabla_version, $registro);
    }
}
