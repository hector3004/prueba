<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="assets/images/favicon.png">

	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/style.css" rel="stylesheet" type="text/css">
	<link href="assets/css/main_style.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet">
	<!-- jQuery  -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>
	<script src="assets/js/metismenu.min.js"></script>
	<script src="assets/js/jquery.slimscroll.js"></script>
	<script src="plugins/ajaxForm/ajaxForm.min.js"></script>
	<script src="plugins/tooltip/tooltip.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<link href="assets/css/dataTable.css" rel="stylesheet" type="text/css">
</head>
<body>

<div id="container" class="container">
	<h1>Prueba Hector H. Cedillo Alvarado.</h1>

	<div id="body">
		<form class="row" id="form-prueba" autocomplete="off" >
			<div class="col-md-6 col-lg-6">
				<label class="label">Marca</label>
				<input type="text" name="marca" class="form-control marca" autocomplete="off">
			</div>
			<div class="col-md-6 col-lg-6">
				<label class="label">Tipo</label>
				<select name="tipo" class="form-control tipo">
				</select>
			</div>
			<div class="col-md-6 col-lg-6">
				<label class="label">Modelo</label>
				<select name="categorias" class="form-control modelo">
				</select>
			</div>
			<div class="col-md-6 col-lg-6">
				<label class="label">Categoria</label>
				<select name="modelo" class="form-control categorias">
				</select>
			</div>
			<div class="col-md-6 col-lg-6">
				<label class="label">Version</label>
				<select name="version" class="form-control version">
				</select>
			</div>
			<div class="col-md-6 col-lg-6">
				<label class="label">Detalle</label>
				<select name="detalle" class="form-control detalle">
				</select>
			</div>
			<div class="col-md-6 col-lg-6">
				<button type="submit" class="btn btn-dark">
					<span class="fa fa-save"></span>
					Guardar
				</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	let marca, tipo, modelo, categoria, version;
	$(document).ready(function () {
		$('.marca').change(function () {
			marca = $(this).val();
			url = 'https://private-7475d3-benditocoche.apiary-mock.com/asesores/libroazul/tipo.json?m={aveo}';

			$.ajax({
				'url' : url,
				'type' : 'GET',
				success : function (data) {
					opcion = '<option value="">Seleccione..</option>';
					$('.tipo').empty().append(opcion);

					$.each(data, function (index, element){
						select = '<option value="'+element.value+'">'+ element.description +'</option>';
						$('.tipo').append(select);
					});
				}
			});
		});
		$('.tipo').change(function (e) {
			tipo = $(this).val();
			url = 'https://private-7475d3-benditocoche.apiary-mock.com/asesores/libroazul/modelo.json?m={'+ marca +'}&o={'+ tipo +'}';
			$.ajax({
				'url' : url,
				'type' : 'GET',
				success : function (data) {
					opcion = '<option value="">Seleccione..</option>';
					$('.modelo').empty().append(opcion);
					$.each(data, function (index, element){
						select = '<option value="'+element.value+'">'+ element.description +'</option>';
						$('.modelo').append(select);
					});
				}
			});
		});
		$('.modelo').change(function (e) {
			modelo = $(this).val();
			url = 'https://private-7475d3-benditocoche.apiary-mock.com/asesores/libroazul/categoria.json?m={'+ marca +'}&o={'+ tipo +'}&a={'+ modelo +'}';
			$.ajax({
				'url' : url,
				'type' : 'GET',
				success : function (data) {
					opcion = '<option value="">Seleccione..</option>';
					$('.categorias').empty().append(opcion);
					$.each(data, function (index, element){
						select = '<option value="'+element.value+'">'+ element.description +'</option>';
						$('.categorias').append(select);
					});
				}
			});
		});

		$('.categorias').change(function (e) {
			categoria = $(this).val();
			url = 'https://private-7475d3-benditocoche.apiary-mock.com/asesores/libroazul/version.json?m={'+ marca +'}&o={'+ tipo +'}&a={'+ modelo +'}&t={'+ categoria +'}';
			$.ajax({
				'url' : url,
				'type' : 'GET',
				success : function (data) {
					opcion = '<option value="">Seleccione..</option>';
					$('.version').empty().append(opcion);
					$.each(data, function (index, element){
						select = '<option value="'+element.value+'">'+ element.description +'</option>';
						$('.version').append(select);
					});
				}
			});
		});
		$('.version').change(function (e) {
			version = $(this).val();
			url = 'https://private-7475d3-benditocoche.apiary-mock.com/asesores/libroazul/detalle.json?m={'+marca+'}&o={'+tipo+'}&a={'+modelo+'}&t={'+categoria+'}&c={'+version+'}';
			$.ajax({
				'url' : url,
				'type' : 'GET',
				success : function (data) {
					opcion = '<option value="">Seleccione..</option>';
					$('.detalle').empty().append(opcion);
					$.each(data, function (index, element){
						select = '<option value="'+element.pventa+'">'+ element.pcompra +'</option>';
						$('.detalle').append(select);
					});
				}
			});
		});

		$('#form-prueba').ajaxForm({
			url : 'https://private-anon-4e75fb9cac-benditocoche.apiary-mock.com/asesores/curlbendito.json';
			cache: false,
			type : 'POST',
			success : function (data) {
				console.log(data);
			}
		});
	});

</script>
</body>
</html>
